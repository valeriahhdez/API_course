
# Surf report API 

## 1. Resource

/surfreport/<span style= "color: blue">{beachId}</span>


<!-- Start the description with a verb. 
1-3 sentences maximum -->
Contains information about surfing conditions like tide, wind, water temperature, and wave height. It also gives recommendations about whether to go surfind. 


## 2. Endpoints and methods

**GET** &nbsp; &nbsp; &nbsp; /surfreport/<span style= "color: blue">{beachId}</span>

Gets the surf conditions for a specific beach ID. 

## 3. Parameters

### Path parameters

| Parameter  | Type | Required/optional | Description |
| :----------- | :---------- |  :----------- | :------------ |
| <span style= "color: blue">{beachId}</span>    | Integer  | Required | The specific ID number of the beach you want to extract weather conditions from |

<br>

### Query string parameters


| Parameter  | Type | Required/optional | Description |
| :----------- | :---------- |  :----------- | :------------ |
| days    | Integer  | Optional | Number of days for which you want to extract weather conditions information. <br> Default value = 3, <br> Min = 1, Max = 7 |
| units    | String  | Optional | The units in which you want the information to be displayed. <br> Values = metric or imperial.   |
| time    | Integer. Unix format (ms since 1970) in UTC  | Optional | If you include the time, the response will include weather conditions for the given hour.    |

<br>
<br>
## 4. Request sample

```
curl -I -X GET "https://api.openweathermap.org/data/2.5/surfreport?zip=95050&appid=APIKEY&units=imperial&days=2"
```
In the above code replace _APIKEY_ with your own API key. 
